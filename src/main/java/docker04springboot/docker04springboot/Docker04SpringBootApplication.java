package docker04springboot.docker04springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Docker04SpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(Docker04SpringBootApplication.class, args);
	}

}
