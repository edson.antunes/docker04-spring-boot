package docker04springboot.docker04springboot.resource;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/docker/hello")


public class HelloResource {

    @GetMapping
    public String hello() { return "Docker rodando o Spring Boot Versao 2.14 - subindo automaticamente !!!"; }

}
