FROM openjdk:12
ADD target/docker04-spring-boot.jar docker04-spring-boot.jar
EXPOSE 8086
ENTRYPOINT ["java", "-jar", "docker04-spring-boot.jar"]