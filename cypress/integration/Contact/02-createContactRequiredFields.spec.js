const selector = require('../../fixtures/selectors.json');

describe('Contact creation Require fields validation: Contact Form test', function(){
    it('Contact creation Require fields validation: Contact form flow', function(){
        cy.visit(selector.urlApp)
        cy.get(`a[href="${selector.contacts}"]`).first().click()
        cy.get(`a[href="${selector.createContacts}"]`).first().click()
        cy.location('pathname', {timeout: 6000})
        .should('include', selector.contacts);

        cy.get(selector.firstNameId).clear()
        .should('have.value', '')

        cy.get(selector.titleId).clear()
        .should('have.value', '')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.get('input:invalid').should('have.length', 2)
    })
})