const selector = require('../../fixtures/selectors.json');

describe('Contact Delete test', function(){
    it('Delete contact flow', function(){
         cy.visit(selector.urlApp)
         cy.get(`a[href="${selector.contacts}"]`).first().click()
       
         let contactsSize
         cy.get(`.ac-contact__header`).should("contain.text", "Automated").then((contacts) => {
            contactsSize = contacts.length

            cy.get(`.ac-contact__header`).should("contain.text", "Automated").children(".ac-contact__actions")
                .children(".stretched-link").first().click({ force: true })

            cy.contains(selector.delete).click()
            cy.contains(selector.deleteContactConfirmMessage).click({ force: true })

            cy.get(`a[href="${selector.contacts}"]`).first().click()
            if(contactsSize > 1){
                cy.get(selector.selectContactCardClass).children().children().its('length').should('eq', contactsSize-1)
            } else {
                cy.get(`.ac-contact__header`).should('not.exist')
            }
         })
    })
})