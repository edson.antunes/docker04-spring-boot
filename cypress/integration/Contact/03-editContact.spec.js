const selector = require('../../fixtures/selectors.json');

describe('Contact Edit test', function(){
    it('Edit contact flow', function(){

        cy.visit(selector.urlApp)
        cy.get(`a[href="${selector.contacts}"]`).first().click()

        cy.get(`.ac-contact__header`).should("contain.text", "Automated Test")
            .children(".ac-contact__actions")
            .children(".stretched-link").first().click({ force: true })
        cy.location('pathname', {timeout: 8000})

        cy.get(selector.firstNameId).clear()
        .type('Automated')
        .should('have.value', 'Automated')
        const lastName = `Edit test ` + Math.floor((Math.random() * 1000) + 1)
        const fullName = `Automated ` + lastName
        cy.get(selector.lastNameId).clear()
        .type(lastName)
        .should('have.value', lastName)
        
        cy.get(selector.titleId).clear()
        .type('Auto')
        .should('have.value', 'Auto')

        cy.get(selector.selectAccountClass).first().click()
        .find('input')
        .type('{enter}')

        cy.get(selector.emailId).clear()
        .type('automatedTestEdit@avenuecode.com')
        .should('have.value', 'automatedTestEdit@avenuecode.com')

        cy.get(selector.otherEmailId).clear()
        .type('automatedTestEdit@avenuecode.com')
        .should('have.value', 'automatedTestEdit@avenuecode.com')
        
        cy.get(selector.phoneId).clear()
        .type('+55 31 1232-1231')
        .should('have.value', '+55 31 1232-1231')
        
        cy.get(selector.mobilePhoneId).clear()
        .type('+55 31 9999-9999')
        .should('have.value', '+55 31 9999-9999')

        cy.get(selector.linkedinId).clear()
        .type('https://www.linkedin.com/in/autotestedit')
        .should('have.value', 'https://www.linkedin.com/in/autotestedit')

        cy.get(selector.keyContactId).check()
  
        cy.get(`button[type="${selector.submit}"]`).click()

        cy.get(`a[href="${selector.contacts}"]`).first().click()
        
        cy.get(`.ac-contacts-container__body`)
        .contains('span', fullName).should("be.visible")

    })
})