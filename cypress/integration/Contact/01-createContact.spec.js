const selector = require('../../fixtures/selectors.json');

describe('Contact creation test', function(){
    it('Creation contact flow', function(){
        cy.visit(selector.urlApp)
        cy.get(`a[href="${selector.contacts}"]`).first().click()
        cy.get(`a[href="${selector.createContacts}"]`).first().click()
        cy.location('pathname', {timeout: 6000})
        .should('include', selector.createContacts)
        const lastName = `Test `+ Math.floor((Math.random() * 1000) + 1)
        const fullName = `Automated ` + lastName
        
        cy.get(selector.firstNameId).type('Automated')
        .should('have.value', 'Automated')

        cy.get(selector.lastNameId).type(lastName)
        .should('have.value', lastName)
        
        cy.get(selector.titleId).type('Auto')
        .should('have.value', 'Auto')

        cy.get(selector.selectAccountClass).first().click()
        .find('input')
        .type('{enter}')

        cy.get(selector.emailId).type('automatedTest@avenuecode.com')
        .should('have.value', 'automatedTest@avenuecode.com')

        cy.get(selector.otherEmailId).type('automatedTest@avenuecode.com')
        .should('have.value', 'automatedTest@avenuecode.com')
        
        cy.get(selector.phoneId).type('+55 31 1232-1231')
        .should('have.value', '+55 31 1232-1231')
        
        cy.get(selector.mobilePhoneId).type('+55 31 9999-9999')
        .should('have.value', '+55 31 9999-9999')

        cy.get(selector.linkedinId).type('https://www.linkedin.com/in/autotest')
        .should('have.value', 'https://www.linkedin.com/in/autotest')

        cy.get(selector.keyContactId).check()
  
        cy.get(`button[type="${selector.submit}"]`).click()

        cy.get(`a[href="${selector.contacts}"]`).first().click()
        
        cy.get(`.ac-contacts-container__body`)
        .contains('span', fullName).should("be.visible")

    })
})