const selector = require('../../fixtures/selectors.json');


describe('Account creation test', function(){

    it('Creation account flow', function(){
        cy.visit(selector.urlApp)
        cy.get(`a[href="${selector.accounts}"]`).first().click()

        cy.get(`a[href="${selector.createAccounts}"]`).first().click()
        cy.location('pathname', {timeout: 6000})

        const businessName = `Automated Test `+ Math.floor((Math.random() * 1000) + 1);

        cy.get(`input[label="${selector.businessNameLabel}"]`).type(businessName)
        .should('have.value', businessName)

        cy.get(selector.accountStatusId).select('Active')
        .should('have.value', 'Active')

        cy.get(`input[label="${selector.legalNameLabel}"]`).type('automated Test name')
        .should('have.value', 'automated Test name')
    
        cy.get(`input[label="${selector.legalEntityLabel}"]`).type('legal')
        .should('have.value', 'legal')
 
        cy.get(selector.countryNameId).select('Brazil')
        .should('have.value', 'Brazil')

        cy.get(`input[label="${selector.accountIdLabel}"]`).type('123445')
        .should('have.value', '123445')

        cy.get(selector.accountCountryCnpjItin).type('08.542.578/0001-81')
        .should('have.value', '08.542.578/0001-81')

        
        cy.get(`input[label="${selector.phoneLabel}"]`).type('+55 31999123312')
        .should('have.value', '+55 31999123312')

        
        cy.get(`input[label="${selector.websiteLabel}"]`).type('http://www.automatedTest.com.br')
        .should('have.value', 'http://www.automatedTest.com.br')

        cy.get(selector.selectManagerClass)
        .click()
        .find('input')
        .type('{enter}')
        .focus()

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 6000})
        .should('include', selector.createAccounts)

        cy.get(selector.industrySectorId).select('Financial')
        .should('have.value', 'Financial')

        cy.get(selector.employeesCountId).select('26-100')
        .should('have.value', '26-100')

        cy.get(selector.annualRevenueId).select('Less than $1M')
        .should('have.value', 'Less than $1M')
        
        cy.get(selector.observationsId).type('automated observation.')
        .should('have.value', 'automated observation.')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', '/accounts/create')

        
        cy.get(`input[label="${selector.zipCodeLabel}"]`).type('30121-120')
        .should('have.value','30121-120')

        cy.get(`input[label="${selector.addressLineOneLabel}"]`).type('automated street')
        .should('have.value', 'automated street')

        
        cy.get(`input[label="${selector.addressLineTwoLabel}"]`).type('automated square.')
        .should('have.value', 'automated square.')

        cy.get(selector.billingCountryId).select('Brasil')
        .should('have.value', 'Brasil')

        cy.get(selector.billingStateId).select('Sao Paulo')
        .should('have.value', 'Sao Paulo')

        cy.get(selector.billingCityId).select('Osasco')
        .should('have.value', 'Osasco')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', selector.createAccounts)

        cy.get(selector.shippingZipId).type('30121-120')
        .should('have.value','30121-120')

        cy.get(selector.shippingAddressOneId).type('automated street')
        .should('have.value', 'automated street')

        cy.get(selector.shippingAddressTwoId).type('automated square.')
        .should('have.value', 'automated square.')

        cy.get(selector.shippingCountryId).select('Brasil')
        .should('have.value', 'Brasil')

        cy.get(selector.shippingStateId).select('Sao Paulo')
        .should('have.value', 'Sao Paulo')

        cy.get(selector.shippingCityId).select('Osasco')
        .should('have.value', 'Osasco')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', selector.createAccounts)

        cy.get(selector.officeDescriptionId).select('IT Office')
        .should('have.value','IT Office')

        cy.get(selector.officeZipId).type('30121-120')
        .should('have.value','30121-120')

        cy.get(selector.officeAddressLineOneId).type('automated street')
        .should('have.value', 'automated street')

        cy.get(selector.officeAddressLineTwoId).type('automated square.')
        .should('have.value', 'automated square.')

        cy.get(selector.officeCountryId).select('Brasil')
        .should('have.value', 'Brasil')

        cy.get(selector.officeStateId).select('Sao Paulo')
        .should('have.value', 'Sao Paulo')

        cy.get(selector.officeCityId).select('Osasco')
        .should('have.value', 'Osasco')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', selector.createAccounts)

        cy.get(selector.businessUnitId).select('US')
        .should('have.value','US')

        cy.get(selector.invoicingCurrencyId).select('US')
        .should('have.value','US')

        cy.get(selector.purchaseOrderRuleId).select('US')
        .should('have.value','US')

        cy.get(selector.invoicePaymentoTermDaysId).select('45')
        .should('have.value','45')
        
        cy.get(selector.invoiceTemplateId).select('Select one')
        .should('have.value','Select one')

        cy.get(`input[label="${selector.firstNameLabel}"]`).type('Automate first name')
        .should('have.value', 'Automate first name')

        cy.get(`input[label="${selector.lastNameLabel}"]`).type('Automate last name')
        .should('have.value', 'Automate last name')
   
        cy.get(`input[label="${selector.emailLabel}"]`).type('automated.user@avenuecode.com')
        .should('have.value', 'automated.user@avenuecode.com')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', selector.accounts)

        cy.get(`tbody[data-testId="flow-table-rows"]`)
        .contains('td', businessName).should("be.visible")
    })
})