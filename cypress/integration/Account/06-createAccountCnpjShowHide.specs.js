const selector = require('../../fixtures/selectors.json');

describe('Account creation Require fields validation: Market form test', function(){
    it('Account creation Require fields validation: Market form  flow', function(){
        cy.visit(selector.urlApp)
        cy.get(`a[href="${selector.accounts}"]`).first().click()
        cy.get(`a[href="${selector.createAccounts}"]`).first().click()
        cy.location('pathname', {timeout: 6000})
        .should('include', selector.accounts);

        cy.get(selector.countryNameId).select('Brazil')
        .should('have.value', 'Brazil')

        cy.get(selector.accountCountryCnpjItin)
        .should('exist')

        cy.get(selector.countryNameId).select('Canada')
        .should('have.value', 'Canada')

        cy.get(selector.accountCountryCnpjItin)
        .should('not.exist')

        cy.get(selector.countryNameId).select('United States')
        .should('have.value', 'United States')

        cy.get(selector.accountCountryCnpjItin)
        .should('not.exist')

        cy.get(selector.countryNameId).select('Other')
        .should('have.value', 'Other')

        cy.get(selector.accountCountryCnpjItin)
        .should('not.exist')
    })
})