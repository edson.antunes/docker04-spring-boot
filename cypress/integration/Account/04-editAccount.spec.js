const selector = require('../../fixtures/selectors.json');

describe('Edit account test', function(){
    it('Edit account flow', function(){
        cy.visit(selector.urlApp)
        cy.get(`a[href="${selector.accounts}"]`).first().click()
        cy.location('pathname', {timeout: 6000})
        .should('include', selector.accounts);

        cy.get(`tbody[data-testId="flow-table-rows"]`)
            .contains('td', "Automated Test").then((row) =>{
                cy.get(row).parent().get(`button:contains("${selector.edit}")`).first().click()
        })
        cy.get(`button:contains("${selector.editAccount}")`).first().click()

        cy.get(`input[label="${selector.businessNameLabel}"]`)
        .clear().type('Automated Test Edited')
        .should('have.value', 'Automated Test Edited')

        cy.get(selector.accountStatusId).select('Active')
        .should('have.value', 'Active')

        cy.get(`input[label="${selector.legalNameLabel}"]`).clear()
        .type('automated Test name edited')
        .should('have.value', 'automated Test name edited')

        cy.get(`input[label="${selector.legalEntityLabel}"]`).clear()
        .type('legal')
        .should('have.value', 'legal')
 
        cy.get(selector.countryNameId).select('Brazil')
        .should('have.value', 'Brazil')

        cy.get(`input[label="${selector.accountIdLabel}"]`).clear()
        .type('12344500')
        .should('have.value', '12344500')

        cy.get(selector.accountCountryCnpjItin).clear()
        .type('08.542.578/0001-82')
        .should('have.value', '08.542.578/0001-82')

        cy.get(`input[label="${selector.phoneLabel}"]`).clear().type('+55 31999122222')
        .should('have.value', '+55 31999122222')

        cy.get(`input[label="${selector.websiteLabel}"]`).clear().type('http://www.automatedTestEdit.com.br')
        .should('have.value', 'http://www.automatedTestEdit.com.br')

        cy.get(selector.selectManagerClearClass)
        .click()
        cy.location('pathname', {timeout: 6000})
        .should('include', selector.editAccounts)

        cy.get(selector.selectManagerClass)
        .click()
        .find('input')
        .type('{enter}')
        .focus()
    
        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 6000})
        .should('include', selector.editAccounts)

        cy.get(selector.industrySectorId).select('Financial')
        .should('have.value', 'Financial')

        cy.get(selector.employeesCountId).select('26-100')
        .should('have.value', '26-100')

        cy.get(selector.annualRevenueId).select('Less than $1M')
        .should('have.value', 'Less than $1M')
        
        cy.get(selector.observationsId).clear()
        .type('automated observation edited.')
        .should('have.value', 'automated observation edited.')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', selector.editAccounts)

        cy.get(`input[label="${selector.zipCodeLabel}"]`).clear()
        .type('30121-125')
        .should('have.value','30121-125')

        cy.get(`input[label="${selector.addressLineOneLabel}"]`).clear()
        .type('automated street edited')
        .should('have.value', 'automated street edited')

        cy.get(`input[label="${selector.addressLineTwoLabel}"]`).clear()
        .type('automated square edited.')
        .should('have.value', 'automated square edited.')

        cy.get(selector.billingCountryId).select('Brasil')
        .should('have.value', 'Brasil')

        cy.get(selector.billingStateId).select('Sao Paulo')
        .should('have.value', 'Sao Paulo')

        cy.get(selector.billingCityId).select('Osasco')
        .should('have.value', 'Osasco')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', selector.editAccounts)

        cy.get(selector.shippingZipId).clear().type('30121-125')
        .should('have.value','30121-125')

        cy.get(selector.shippingAddressOneId).clear()
        .type('automated street edited.')
        .should('have.value', 'automated street edited.')

        cy.get(selector.shippingAddressTwoId).clear()
        .type('automated square edited.')
        .should('have.value', 'automated square edited.')

        cy.get(selector.shippingCountryId).select('Brasil')
        .should('have.value', 'Brasil')

        cy.get(selector.shippingStateId).select('Sao Paulo')
        .should('have.value', 'Sao Paulo')

        cy.get(selector.shippingCityId).select('Osasco')
        .should('have.value', 'Osasco')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', selector.editAccounts)

        cy.get(selector.officeDescriptionId).select('IT Office')
        .should('have.value','IT Office')

        cy.get(selector.officeZipId).clear().type('30121-120')
        .should('have.value','30121-120')

        cy.get(selector.officeAddressLineOneId).clear()
        .type('automated street edited')
        .should('have.value', 'automated street edited')

        cy.get(selector.officeAddressLineTwoId).clear()
        .type('automated square edited.')
        .should('have.value', 'automated square edited.')

        cy.get(selector.officeCountryId).select('Brasil')
        .should('have.value', 'Brasil')

        cy.get(selector.officeStateId).select('Sao Paulo')
        .should('have.value', 'Sao Paulo')

        cy.get(selector.officeCityId).select('Osasco')
        .should('have.value', 'Osasco')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', selector.editAccounts)

        cy.get(selector.businessUnitId).select('US')
        .should('have.value','US')

        cy.get(selector.invoicingCurrencyId).select('US')
        .should('have.value','US')

        cy.get(selector.purchaseOrderRuleId).select('US')
        .should('have.value','US')

        cy.get(selector.invoicePaymentoTermDaysId).select('45')
        .should('have.value','45')
        
        cy.get(selector.invoiceTemplateId).select('Select one')
        .should('have.value','Select one')

        cy.get(`input[label="${selector.firstNameLabel}"]`).clear()
        .type('Automate first name edit')
        .should('have.value', 'Automate first name edit')

        cy.get(`input[label="${selector.lastNameLabel}"]`).clear()
        .type('Automate last name')
        .should('have.value', 'Automate last name')
        
        cy.get(`input[label="${selector.emailLabel}"]`).clear()
        .type('automated.edit@avenuecode.com')
        .should('have.value', 'automated.edit@avenuecode.com')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 8000})
        .should('include', selector.editAccounts)

        cy.get(`tbody[data-testId="flow-table-rows"]`)
        .contains('td', 'Automated Test').should("be.visible")
    })
})