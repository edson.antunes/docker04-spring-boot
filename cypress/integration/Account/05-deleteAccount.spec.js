const selector = require('../../fixtures/selectors.json');

describe('Delete account test', function(){
    it('Delete account flow', function(){
        cy.visit(selector.urlApp)
        cy.get(`a[href="${selector.accounts}"]`).first().click()
        cy.location('pathname', {timeout: 6000})
        .should('include', selector.accounts);
        cy.get(`tbody[data-testId="flow-table-rows"]`)
            .contains('td', "Automated Test").then((row) =>{
                cy.get(row).parent().get(`button:contains("${selector.edit}")`).first().click()
        })
        
        cy.get(`button:contains("${selector.editAccount}")`).first().click()
        cy.get(`input[label="${selector.businessNameLabel}"]`).invoke('val').then((businessName) =>{
            cy.get(`button:contains("${selector.deleteAccount}")`).first().click()
        
             cy.contains(selector.deleteAccountConfirmMessage).click({ force: true })
             cy.location('pathname', {timeout: 8000})
             
             cy.get(`tbody[data-testId="flow-table-rows"]`)
            .contains('td', businessName).should('not.exist')

        })
    })
})