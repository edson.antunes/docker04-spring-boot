const selector = require('../../fixtures/selectors.json');

describe('Account creation Require fields validation: Market form test', function(){
    it('Account creation Require fields validation: Market form  flow', function(){
        cy.visit(selector.urlApp)
        cy.get(`a[href="${selector.accounts}"]`).first().click()
        cy.get(`a[href="${selector.createAccounts}"]`).first().click()
        cy.location('pathname', {timeout: 6000})
        .should('include', selector.accounts);

        cy.get(`input[label="${selector.businessNameLabel}"]`).type('automatedTest@avenuecode.com')
        .should('have.value', 'automatedTest@avenuecode.com')

        cy.get(selector.accountStatusId).select('Active')
        .should('have.value', 'Active')

        cy.get(`input[label="${selector.legalNameLabel}"]`).type('automated Test name')
        .should('have.value', 'automated Test name')

        cy.get(`input[label="${selector.legalEntityLabel}"]`).type('legal')
        .should('have.value', 'legal')
 
        cy.get(selector.countryNameId).select('Brazil')
        .should('have.value', 'Brazil')

        cy.get(`input[label="${selector.accountIdLabel}"]`).type('123445')
        .should('have.value', '123445')

        cy.get(selector.accountCountryCnpjItin)
        .type('08.542.578/0001-81')
        .should('have.value', '08.542.578/0001-81')

        cy.get(`input[label="${selector.phoneLabel}"]`).type('+55 31999123312')
        .should('have.value', '+55 31999123312')

        cy.get(`input[label="${selector.websiteLabel}"]`)
        .type('http://www.automatedTest.com.br')
        .should('have.value', 'http://www.automatedTest.com.br')

        cy.get(selector.selectManagerClass)
        .click()
        .find('input')
        .type('{enter}')
        .focus()

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.location('pathname', {timeout: 6000})
        .should('include', selector.createAccounts)

        cy.get(selector.industrySectorId).select('')
        .should('have.value', '')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.get('select:invalid').should('have.length', 1)

    })
})