const selector = require('../../fixtures/selectors.json');

describe('Account creation Require fields validation: Account Form test', function(){
    it('Account creation Require fields validation: account form flow', function(){
        cy.visit(selector.urlApp)
        cy.get(`a[href="${selector.accounts}"]`).first().click()
        cy.get(`a[href="${selector.createAccounts}"]`).first().click()
        cy.location('pathname', {timeout: 6000})
        .should('include', selector.accounts);

        cy.get(`input[label="${selector.businessNameLabel}"]`).clear()
        .should('have.value', '')

        cy.get(selector.accountStatusId).select('')
        .should('have.value', '')

        cy.get(`input[label="${selector.legalNameLabel}"]`).clear()
        .should('have.value', '')

        cy.get(`input[label="${selector.legalEntityLabel}"]`).clear()
        .should('have.value', '')
 
        cy.get(selector.countryNameId)
        .should('have.value', '')

        cy.get(`button[type="${selector.submit}"]`).click()

        cy.get('input:invalid').should('have.length', 3)
        cy.get('select:invalid').should('have.length', 2)
    })
})


